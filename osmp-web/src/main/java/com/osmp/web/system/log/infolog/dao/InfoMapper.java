package com.osmp.web.system.log.infolog.dao;

import com.osmp.web.core.mybatis.BaseMapper;
import com.osmp.web.system.log.infolog.entity.InfoLog;

/**
 * Description:
 * 
 * @author: wangkaiping
 * @date: 2014年11月24日 上午10:12:32
 */
public interface InfoMapper extends BaseMapper<InfoLog> {

}
